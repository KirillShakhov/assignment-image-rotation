#ifndef LAB3_STATUS_H
#define LAB3_STATUS_H

#include <stdio.h>




typedef enum {
    READ = 0,
    WRITE,
    CONVERT,
    ROTATE
} status_type;

typedef enum {
    OK = 0,
    INVALID_HEADER,
    NULL_ERROR,
    CLOSE_ERROR,
    ERROR
} status_body;

struct status{
    status_type type;
    status_body body;
};

void print_status(struct status status);

#endif
