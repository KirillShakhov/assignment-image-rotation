#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

bool malloc_image(struct image *img);
void free_image(struct image *img);

#endif
