#ifndef LAB3_BMP_H
#define LAB3_BMP_H

#include "../include/image.h"
#include "../include/status.h"
#include  <stdint.h>
#include <stdio.h>

struct status to_bmp(FILE *file_out, const struct image *img);
struct status from_bmp(FILE *file_in, struct image *img);

#endif
