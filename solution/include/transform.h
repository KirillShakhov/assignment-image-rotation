#ifndef LAB3_TRANSFORM_H
#define LAB3_TRANSFORM_H

#include "image.h"
#include "status.h"

struct optional_image{
    struct image img;
    struct status status;
};

struct optional_image rotate(struct image img, int degree);

#endif
