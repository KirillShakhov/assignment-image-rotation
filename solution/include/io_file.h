#ifndef LAB3_FILE_H
#define LAB3_FILE_H

#include "../include/status.h"
#include <stdbool.h>
#include <stdio.h>



struct status open_file(FILE **file, const char *name, const char *mode);
struct status close_file(FILE **file);

#endif
