#include "../include/transform.h"

#ifdef __MACH__

#include <stdlib.h>

#else
#include <malloc.h>
#endif

 struct image rotate_left_90(const struct image img) {
    unsigned long long size = sizeof(struct pixel) * img.height * img.width;
    if (size != 0) {
        struct pixel *pixels = malloc(size);
        if(pixels != NULL) {
            struct image img1;
            img1.width = img.height;
            img1.height = img.width;
            img1.data = pixels;
            for (size_t i = 0; i < img.height; i++) {
                for (size_t j = 0; j < img.width; j++) {
                    *(pixels + j * img.height + (img.height - 1 - i)) = *(img.data + i * img.width + j);
                }
            }
            return img1;
        }
    }
    return img;
}

struct optional_image rotate(struct image img, int degree) {
    struct optional_image result;
    result.status.type = ROTATE;
    result.status.body = OK;
    if ((degree % 360) == 0 || degree == 0) {
        result.img = img;
    } else if (degree == -90 || degree == 270) {
        result.img = rotate_left_90(img);
    } else if (degree == -180 || degree == 180) {
        struct image img1 = rotate_left_90(img);
        result.img = rotate_left_90(img1);
        free_image(&img1);
    } else if (degree == -270 || degree == 90) {
        struct image img1 = rotate_left_90(img);
        struct image img2 = rotate_left_90(img1);
        result.img = rotate_left_90(img2);
        free_image(&img1);
        free_image(&img2);
    } else {
        result.img = img;
        result.status.body = ERROR;
    }
    return result;
}
