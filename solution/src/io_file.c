#include "../include/io_file.h"

struct status open_file(FILE **file, const char *name, const char *mode) {
    if (file != NULL) {
        *file = fopen(name, mode);
        if(*file == NULL) return (struct status){.type = READ, .body = *file == NULL};
    }
    return (struct status){.type = READ, .body = OK};
}

struct status close_file(FILE **file) {
    if (!*file) {
        if (fclose(*file) != 0) return (struct status) {.type = WRITE, .body = ERROR};
    }
    return (struct status) {.type = WRITE, .body = OK};
}
