#include "../include/image.h"

bool malloc_image(struct image *img){
    if (img != NULL) {
        img->data = malloc(sizeof(struct pixel) * img->width * img->height);
        return img->data != NULL;
    }
    return false;
}

void free_image(struct image *img){
    if (img != NULL) free(img->data);
}
