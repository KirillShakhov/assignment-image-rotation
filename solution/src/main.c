#include "../include/bmp.h"
#include "../include/io_file.h"
#include "../include/transform.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    char *path_input_file = NULL, *path_output_file = NULL;
    struct image img;
    int degree = -90;
    if (argc < 3 || argc > 4) {
        fprintf(stderr, "You have specified the wrong number of arguments\n");
        return 1;
    }
    path_input_file = argv[1];
    path_output_file = argv[2];
    if(argc >= 4) strtol(argv[3], (char **) NULL, 10);

    FILE *input_file = NULL, *output_file = NULL;
    print_status(open_file(&input_file, path_input_file, "rb"));
    print_status(open_file(&output_file, path_output_file, "wb"));
    print_status(from_bmp(input_file, &img));

    struct optional_image new_image = rotate(img, degree);
    print_status(new_image.status);
    print_status(to_bmp(output_file, &new_image.img));
    print_status(close_file(&input_file));
    print_status(close_file(&output_file));
    free_image(&img);
    free_image(&new_image.img);
    return 0;
}
